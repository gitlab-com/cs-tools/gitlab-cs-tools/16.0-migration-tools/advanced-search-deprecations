# Projects you want to check for deprecations
#projects:
#  - group/project
# Group you want to check for deprecations. Retrieves projects from all subgroups
groups:
  - sciops
# if neither groups or projects are defined, all groups the token has access to will be retrieved (instance mode)
# criteria formalize deprecated features
criteria:
  jwt_token_depr:
    name: "CI_JOB_JWT 1/2 deprecated"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#old-versions-of-json-web-tokens-are-deprecated
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/366798
    search:
      - 'CI_JOB_JWT'
      - 'CI_JOB_JWT_V2'
  dast_ZAP_variables:
    name: "DAST ZAP advanced configuration variables deprecation"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#dast-zap-advanced-configuration-variables-deprecation
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/383467
    search:
      - 'DAST_API_HOST_OVERRIDE'
      - 'DAST_API_SPECIFICATION'
  file_type_variable_expansion_in_downstream_pipelines:
    name: "File type variable expansion fixed in downstream pipelines"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#file-type-variable-expansion-fixed-in-downstream-pipelines
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/419445
    search:
      - 'trigger:'
    type: "yml"
    comment: "This will only highlight projects that have downstream pipelines defined. File-type variable usage needs to be checked separately for indicated projects."
  after_script_cancelled_jobs:
    name: "after_script keyword will run for cancelled jobs"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#after_script-keyword-will-run-for-cancelled-jobs
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/437789
    search:
      - 'after_script:'
    type: "yml"
    comment: "Highlights usage of after_script, have to check manually if it will be affected by running for cancelled jobs."
  terraform_fmt:
    name: "Deprecate fmt job in Terraform Module CI/CD template"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#deprecate-fmt-job-in-terraform-module-cicd-template
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/440249
    search:
      - 'Terraform-Module.gitlab-ci.yml'
      - 'Terraform/Module-Base.gitlab-ci.yml'
    comment: "Highlights usage of terraform templates. The removed fmt job is best practice, so consider adding it manually."
  saas_windows_runners:
    name: "Deprecating Windows Server 2019 in favor of 2022"
    doc: https://docs.gitlab.com/ee/update/deprecations.html?removal_milestone=17.0#deprecating-windows-server-2019-in-favor-of-2022
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/438554
    search:
      - 'shared-windows'
      - 'windows-1809'
    type: "yml"
    comment: "Replace with saas-windows-medium-amd64"
  saas_linux_runners:
    name: "Removal of tags from small SaaS runners on Linux"
    doc: https://docs.gitlab.com/ee/update/deprecations.html?removal_milestone=17.0#removal-of-tags-from-small-saas-runners-on-linux
    issue: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/30829
    search:
      - '- docker'
      - '- east-c'
      - '- git-annex'
      - '- linux'
      - '- mongo'
      - '- mysql'
      - '- postgres'
      - '- ruby'
      - '- shared'
    type: "yml"
    comment: "Large potential of false positives here. Replace with saas-linux-small-amd64."
  agentk_pull_based:
    name: "The pull-based deployment features of the GitLab agent for Kubernetes is deprecated"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#the-pull-based-deployment-features-of-the-gitlab-agent-for-kubernetes-is-deprecated
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/406545
    search:
      - '.gitlab/agents /config.yaml'
    comment: "May not work well on self-managed advanced search. Only tested for gitlab.com using Zoekt"
  policy_newly_detected:
    name: "Security policy field newly_detected is deprecated"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#security-policy-field-newly_detected-is-deprecated
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/422414
    search:
      - 'newly_detected'
    type: "yml"
  policy_match_on_inclusion:
    name: "Security policy field match_on_inclusion is deprecated"
    doc: https://docs.gitlab.com/ee/update/deprecations.html#security-policy-field-match_on_inclusion-is-deprecated
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/424513
    search:
      - 'match_on_inclusion'
    type: "yml"
  agentk_cacert_file:
    name: "Agent for Kubernetes option ca-cert-file renamed"
    doc: https://docs.gitlab.com/ee/update/deprecations.html?removal_milestone=17.0#agent-for-kubernetes-option-ca-cert-file-renamed
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/437728
    search:
      - 'config.caCert'
    type: "yaml"
  license_scanning_template_dep:
    name: "Deprecate License Scanning CI templates"
    doc: https://docs.gitlab.com/ee/update/deprecations.html?removal_milestone=17.0#deprecate-license-scanning-ci-templates
    issue: https://gitlab.com/gitlab-org/gitlab/-/issues/439157
    search:
      - 'License-Scanning.gitlab-ci.yml'
      - 'License-Scanning.latest.gitlab-ci.yml'
    type: "yml"
    comment: "Replace with Jobs/Dependency-Scanning.gitlab-ci.yml"