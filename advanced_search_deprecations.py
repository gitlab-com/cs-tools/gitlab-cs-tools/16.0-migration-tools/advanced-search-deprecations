#!/usr/bin/env python3

import gitlab
import json
import argparse
import yaml
import collections
import re
import csv

# get all projects for a group
def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        print("Getting all projects for group %s" % topgroup)
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(iterator=True, include_subgroups=True, lazy=True)
        for group_project in group_projects:
            projects.append(gl.projects.get(group_project.id, lazy=True))
    print("Found %s projects" % len(projects))
    return projects

def sort_by_group(project_adoptions):
    # sort groups alphabetically, sort projects in groups alphabetically, produce a list
    sorted_projects = {}
    for project in project_adoptions:
        project_group = project["path"]
        project_group = project_group[0:project_group.rfind("/")]
        project["group"] = project_group
        if project_group in sorted_projects:
            sorted_projects[project_group].append(project)
            sorted_projects[project_group] = sorted(sorted_projects[project_group], key=lambda s: s["name"])
        else:
            sorted_projects[project_group] = [project]
    sorted_projects = collections.OrderedDict(sorted(sorted_projects.items()))
    project_list = []
    for group in sorted_projects:
        projects = sorted_projects[group]
        for project in projects:
            project_list.append(project)
    return project_list

def get_group_count(project_adoptions):
    groups = set()
    for project in project_adoptions:
        groups.add(project["group"])
    return len(groups)

def group_search(gl, groups, criteria, args):
    search_findings = {}
    for group in groups:
        print("[Info] Searching in group %s" % group.full_path)
        for crit_key in criteria:
            criterium = criteria[crit_key]
            findings = search_criterium(gl, group, criterium, crit_key)
            if group.full_path in search_findings:
                search_findings[group.full_path].extend(findings)
            else:
                search_findings[group.full_path] = findings
    return search_findings

def search_criterium(gl, group, criterium, crit_key):
    findings = []
    print("[Info] Searching for %s in group %s" % (crit_key, group.full_path))
    for search_string in criterium["search"]:
        finding = {}
        num_results = 0
        search_results = group.search(gitlab.const.SearchScope.BLOBS, search_string, iterator=True)
        for result in search_results:
            num_results += 1
            if num_results % 100 == 0:
                print("found %s results" % num_results)
            if " " in search_string and not (search_string.startswith('"') and search_string.endswith('"')):
                real_search_string = search_string.split(" ")[0]
                if real_search_string in result["data"]:
                    finding = result
                    finding["criterium"] = crit_key
                    finding["search_string"] = search_string
                    filetype = finding["filename"][finding["filename"].rfind(".")+1:]
                    if "type" in criterium:
                        if filetype == criterium["type"]:
                            findings.append(finding)
                            continue
                    else:
                        findings.append(finding)
                        continue
            if search_string in result["data"]:
                finding = result
                finding["criterium"] = crit_key
                finding["search_string"] = search_string
                filetype = finding["filename"][finding["filename"].rfind(".")+1:]
                if "type" in criterium:
                    if filetype == criterium["type"]:
                        findings.append(finding)
                        continue
                else:
                    findings.append(finding)
                    continue
    return findings

def annotate_findings(gl, search_findings):
    projects = {}
    annotated_findings = {}
    for group in search_findings:
        findings = search_findings[group]
        project_findings = {}
        for finding in findings:
            if finding["project_id"] not in projects:
                project_object = gl.projects.get(finding["project_id"])
                projects[finding["project_id"]] = project_object.path_with_namespace
                finding["project_namespace"] = project_object.path_with_namespace
            else:
                finding["project_namespace"] = projects[finding["project_id"]]
            
            finding["filetype"] = finding["filename"][finding["filename"].rfind(".")+1:]
            if finding["project_namespace"] in project_findings:
                project_findings[finding["project_namespace"]].append(finding)
            else:
                project_findings[finding["project_namespace"]] = [finding]
        annotated_findings[group] = project_findings
    return annotated_findings

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='YML file that defines requested groups, projects and adoption parameters')
parser.add_argument('--gitlab', help='URL of the gitlab instance', default="https://gitlab.com/")

args = parser.parse_args()

gitlab_url = args.gitlab.strip("/") + "/"
gl = gitlab.Gitlab(gitlab_url, private_token=args.token, retry_transient_errors=True)

configfile = args.configfile
groups = []
projects = []
criteria = {}
frameworks = {}
compliance_project = None

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    if "groups" in config:
        for group in config["groups"]:
            groups.append(gl.groups.get(group))
    else:
        print("Retrieving all projects the token has access to")
        groups = gl.groups.list(iterator=True)
    if "criteria" in config:
        criteria = config["criteria"]
    else:
        print("No criteria provided, exiting")

search_findings = group_search(gl, groups, criteria, args)

with open("search_findings.json", "w") as reportfile_json:
    json.dump(search_findings, reportfile_json, indent = 2)

search_findings = annotate_findings(gl, search_findings)

with open("annotated_findings.json", "w") as reportfile_json:
    json.dump(search_findings, reportfile_json, indent = 2)

with open("search_findings.csv", "w") as reportfile:
    writer = csv.writer(reportfile, delimiter="\t")
    header = ["project","deprecation","search_string","finding_text","finding_link","filename","filetype","deprecation_doc","deprecation_issue","ref","startline"]
    writer.writerow(header)
    for group in search_findings:
        for project in search_findings[group]:
            for finding in search_findings[group][project]:
                deprecation_data = criteria[finding["criterium"]]
                finding_url = gitlab_url + project + "/-/tree/" + finding["ref"] + "/" + finding["filename"] + "#L" + str(finding["startline"])
                finding_row = [project, deprecation_data["name"], finding["search_string"], finding["data"], finding_url, finding["filename"], finding["filetype"], deprecation_data["doc"], deprecation_data["issue"], finding["ref"], finding["startline"]]
                writer.writerow(finding_row)

