# Find deprecations using advanced search

Use the advanced search API to search for strings indicating deprecations across GitLab groups.

Config files for 16.0 and 17.0 deprecations are available.

## Usage

### Installing locally or on self-hosted GitLab

`pip3 install -r requirements.txt`

### Running the report

`python3 advanced_search_deprecations.py $GIT_TOKEN $CONFIG_YML --gitlab $GITLAB_URL`

* $GIT_TOKEN: An access token with API and Repository permissions for the configured groups and projects
* $CONFIG_YML: Path to the `config.yml` file containing the configuration
* $GITLAB_URL: Optional URL of the GitLab instance. Defaults to `https://gitlab.com`

**Supporting self-signed certificates**

[Python-gitlab supports self-signed ssl certificates](https://python-gitlab.readthedocs.io/en/stable/api-usage-advanced.html#ssl-certificate-verification). You need provide the path to the CA bundle in the `REQUESTS_CA_BUNDLE` environment variable:

`export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt`

## Configuration

### Define groups or projects

- edit config.yml
  - specify `groups` as arrays using their ID
  - If no groups are defined, all groups the token has access to will be used to search. On self-managed with an admin token, that's equivalent to all projects on the instance

## Included deprecations

The config files contain additional information on which deprecations are supported to be found. Comments supply information about potential limitations (such as false positive rates or the need for further investigation).

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems.
ms.
